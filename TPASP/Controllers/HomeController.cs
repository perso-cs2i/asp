﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TPASP.Models;

namespace TPASP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            //var days = new String[] { "monday", "tuesday" };
            //days.Where(d => d.Contains("a"));
            return View();
        }

        public IActionResult Demo()
        {
            // ViweBag est un dictionnaire .NET
            // => une liste de clefs/valeurs
            ViewBag.FirstName = "Céline";
            ViewBag.Lastname = "LESCOP";

            // on ajoute aussi un objet dans le ViewBag
            ViewBag.Person = new Person("Céline","Lescop");
            return View();

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
