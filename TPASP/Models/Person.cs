﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TPASP.Models
{
    public class Person
    {
        public String FirstName;
        public String LastName;

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public String getFirstName { get; set; }
        public String getLastName { get; set; }
    }
}
